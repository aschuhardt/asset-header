# Asset-Header

A small utility for converting binary files into C headers.

## Syntax

Invocation:

`asset_header.exe file object_name output`

Arguments:

-   file: the path of the file to be process
-   object_name: the name given to the C byte array
-   output: the intended path of the generated header file

Example:

`asset_header.exe human_texture.png TEXTURE_DATA_HUMAN human_tex.h`
