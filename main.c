#include <stdio.h>
#include <stdlib.h>

// the size of the output buffer used for writing data to disk
#define BUFFER_BLOCK_SIZE 4096

#define LINE_LENGTH 8

int main(int argc, const char **argv) {
  if (argc < 4) {
    printf("Asset-header: converts a file into "
           "a byte array in a C header file\n");
    printf("Usage: \"asset_path.exe input_file.png "
           "OBJECT_NAME output_path.h\"\n");
    return EXIT_SUCCESS;
  }

  // open the asset file
  FILE *input_file = fopen(argv[1], "rb");
  if (!input_file) {
    perror("Could not open asset file");
    return EXIT_FAILURE;
  }

  // allocate the input buffer
  unsigned char *buffer = malloc(BUFFER_BLOCK_SIZE * sizeof(unsigned char));

  if (!buffer) {
    printf("Failed to allocate output buffer of size %d\n", BUFFER_BLOCK_SIZE);
  } else {

    // open/create the header file
    FILE *output_file = fopen(argv[3], "wb");
    if (!output_file) {
      perror("Could not create output file");
      return EXIT_FAILURE;
    }

    // calculate the size of the input file
    fseek(input_file, 0, SEEK_END);
    long input_size = ftell(input_file);

    // write the header's prelude (include-guards, array declaration)
    fprintf(output_file, "#ifndef %s_H\n", argv[2]);
    fprintf(output_file, "#define %s_H\n\n", argv[2]);
    fprintf(output_file, "const unsigned char * const %s[%d] = {\n", argv[2],
            input_size);

    // move the input cursor back to the beginning of the file
    fseek(input_file, 0, SEEK_SET);

    long bytes_written = 0;
    for (;;) {
      // try to read BUFFER_BLOCK_SIZE bytes
      size_t bytes_read = fread((void *)buffer, sizeof(unsigned char),
                                BUFFER_BLOCK_SIZE, input_file);

      // write those bytes to the header file formatted as hexidecimal
      for (int i = 0; i < bytes_read; ++i) {
        fprintf(output_file, "0x%02x", buffer[i]);
        if (bytes_written < input_size - 1)
          fputs(", ", output_file);
        if (bytes_written > 0 && (bytes_written + 1) % LINE_LENGTH == 0)
          fputc('\n', output_file);
        ++bytes_written;
      }

      // if the size we read was smaller than our block size, we're done
      if (bytes_read < BUFFER_BLOCK_SIZE) {
        break;
      }
    }

    // wrap up the declaration and include-guard
    fputs("\n};\n", output_file);
    fputs("#endif", output_file);

    fclose(output_file);

    free(buffer);
  }

  fclose(input_file);

  return EXIT_SUCCESS;
}